import { Test, TestingModule } from '@nestjs/testing';
import { WalletController } from '../wallet.controller';
import { WalletService } from '../wallet.service';

describe('WalletController', () => {
  let controller: WalletController;
  let service: WalletService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WalletController],
      providers: [WalletService],
    }).compile();

    controller = module.get<WalletController>(WalletController);
    service = module.get<WalletService>(WalletService);
  });

  describe('getBalance', () => {
    it('should get balance of user', async () => {
      const balance = 100; // set expected balance
      const userID = 5; // set user ID
      jest.spyOn(service, 'getBalance').mockResolvedValue({ balance: balance }); // mock the getBalance method of your service to return your expected balance

      const result = await controller.getBalance({ user_id: userID }); // call the method
      expect(result).toBe(balance); // make assertions
    });
  });

  describe('addBalance', () => {
    it('should add balance to user', async () => {
      const balance = 100; // set expected balance
      const userID = 5; // set user ID
      const amount = 50; // set amount to add

      // mock the addMoney method of your service to return the new balance
      jest.spyOn(service, 'addMoney').mockResolvedValue(balance + amount);

      const result = await controller.addBalance({
        user_id: userID,
        amount: amount,
      }); // call the method
      expect(result).toBe(balance + amount); // make assertions
    });
  });
});
