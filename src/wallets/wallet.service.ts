import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Wallet } from './entities/wallet.entity';
import {
  Connection,
  Equal,
  LessThan,
  QueryFailedError,
  Repository,
} from 'typeorm';
import { UserBalanceResponse } from './responses/get-balance.response';
import { Transaction } from './entities/Transaction.entity';
import { v4 as uuidv4 } from 'uuid';
import { AddBalanceResponse } from './responses/add-balance.response';

@Injectable()
export class WalletService {
  constructor(
    @InjectRepository(Wallet)
    private walletRepository: Repository<Wallet>,
    @InjectRepository(Transaction)
    private transactionRepository: Repository<Transaction>,
    private connection: Connection,
  ) {}
  async getBalance(userId: number): Promise<UserBalanceResponse> {
    const ret = await this.walletRepository.findOne({
      where: { user_id: Equal(userId) },
    });
    if (ret) {
      return {
        balance: ret.balance,
      };
    }
    throw new HttpException('user not founded!', HttpStatus.NOT_FOUND);
  }
  async addMoney(userId: number, amount: number): Promise<AddBalanceResponse> {
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    let t: Transaction;
    try {
      const wallet = await queryRunner.manager.findOne(Wallet, {
        where: { user_id: Equal(userId) },
      });
      await queryRunner.manager.save(Wallet, {
        id: wallet?.id,
        userId: userId,
        balance: wallet?.balance + amount,
      });
      await queryRunner.commitTransaction();
      t = this.transactionRepository.create({
        user_id: userId,
        date: new Date(),
        amount: amount,
        reference_id: uuidv4(),
      });
      await this.transactionRepository.save(t);
    } catch (err) {
      t = this.transactionRepository.create({
        user_id: userId,
        date: new Date(),
        describe: `${err.message} ${err.query}`,
        amount: amount,
        reference_id: uuidv4(),
      });
      await this.transactionRepository.save(t);
      await queryRunner.rollbackTransaction();
      throw new HttpException('transaction failed!', HttpStatus.BAD_REQUEST);
    } finally {
      await queryRunner.release();
    }

    return {
      reference_id: t.reference_id,
    };
  }

  async getTotalAmounts(): Promise<number> {
    const ret = await this.transactionRepository.query(
      'select sum(amount) total from transaction',
    );
    // const rret = await this.transactionRepository.find({
    //   where: {
    //     date: LessThan(new Date()),
    //   },
    // });
    // console.log(rret);

    return +ret[0].total;
  }
}
