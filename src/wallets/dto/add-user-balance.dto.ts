import { IsNotEmpty, IsNumber } from 'class-validator';

export class AddUserBalance {
  @IsNumber({ allowNaN: false })
  @IsNotEmpty()
  user_id: number;

  @IsNumber({ allowNaN: false })
  @IsNotEmpty()
  amount: number;
}
