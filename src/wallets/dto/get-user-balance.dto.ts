import { IsNotEmpty, IsNumber } from 'class-validator';

export class GetUserBalance {
  @IsNumber()
  @IsNotEmpty()
  user_id: number;
}
