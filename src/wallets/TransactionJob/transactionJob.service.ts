import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { WalletService } from '../wallet.service';

console.log(process.env.DAILY_TRANS_CRON);

@Injectable()
export class TransactionJobService {
  constructor(private readonly walletService: WalletService) {}

  @Cron('0/10 * * * * *')
  async handleDailyJob() {
    console.log(
      `total amount is ${await this.walletService.getTotalAmounts()}`,
    );
  }
}
