import { Module } from '@nestjs/common';
import { TransactionJobService } from './transactionJob.service';
import { WalletModule } from '../wallet.module';

@Module({
  imports: [WalletModule],
  providers: [TransactionJobService],
  controllers: [],
  exports: [],
})
export class TransactionJobModule {}
