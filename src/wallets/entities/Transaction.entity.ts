import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Transaction {
  @PrimaryColumn({ type: 'uuid' })
  reference_id: string;

  @Column({ type: Number })
  user_id: number;

  @Column({ type: Date })
  date: Date;

  @Column({ type: Number })
  amount: number;

  @Column({ type: String, default: 'ok' })
  describe: string;
}
