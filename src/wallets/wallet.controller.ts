import { Controller, Get, Post, Param } from '@nestjs/common';
import { WalletService } from './wallet.service';
import { GetUserBalance } from './dto/get-user-balance.dto';
import { AddUserBalance } from './dto/add-user-balance.dto';

@Controller('wallets')
export class WalletController {
  constructor(private readonly walletService: WalletService) {}

  @Get(':user_id/balance')
  async getBalance(@Param() { user_id }: GetUserBalance): Promise<any> {
    return await this.walletService.getBalance(user_id);
  }

  @Post(':user_id/:amount/add-money')
  async addBalance(@Param() { user_id, amount }: AddUserBalance): Promise<any> {
    return await this.walletService.addMoney(user_id, +amount);
  }
}
