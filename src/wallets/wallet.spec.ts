import { HttpException, HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from 'src/app.module';
import { WalletController } from './wallet.controller';
import { WalletService } from './wallet.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Wallet } from './entities/wallet.entity';
import { Repository } from 'typeorm';
import { Transaction } from './entities/Transaction.entity';

const mockFinancialAccountService = () => ({
  findAllPersons: jest.fn(),
  findOnePerson: jest.fn(),
  activatePerson: jest.fn(),
  updatePerson: jest.fn(),
});

describe('WalletController', () => {
  let app: INestApplication;
  let walletController: WalletController;
  let walletService: WalletService;
  let walletRepository: Repository<Wallet>;
  let transactionRepository: Repository<Transaction>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forFeature([Wallet]), AppModule],
      providers: [WalletService],
      controllers: [WalletController],
    }).compile();
    app = module.createNestApplication();
    await app.init();
    walletService = module.get<WalletService>(WalletService);
    walletController = module.get<WalletController>(WalletController);
  });

  describe('initials', () => {
    it('defined persons Controller', async () => {
      expect(walletController).toBeDefined();
    });
    it('defined persons Service', async () => {
      expect(walletService).toBeDefined();
    });
  });

  describe('root', () => {
    // it('first test', async () => {
    //   expect(await walletController.getBalance({ user_id: 1 })).toHaveBeenCalled();
    // });
    it('second test', async () => {
      expect(await walletController.getBalance({ user_id: -1 })).toThrow(
        'user not founded!',
      );
    });
  });

  describe('getBalance', () => {
    it('should return balance of user', async () => {
      const expectedBalance = {
        balance: 100,
      };
      const userID = 5;

      jest.spyOn(walletRepository, 'findOne').mockResolvedValue({
        userId: userID,
        balance: 100,
      });

      const result = await walletService.getBalance(userID);

      expect(result).toEqual(expectedBalance);
    });

    it('should throw not found error', async () => {
      const userID = 2;

      jest.spyOn(walletRepository, 'findOne').mockResolvedValue(undefined);

      await expect(walletService.getBalance(userID)).rejects.toThrow(
        new HttpException('user not founded!', HttpStatus.NOT_FOUND),
      );
    });
  });

  describe('addMoney', () => {
    it('should add balance to user', async () => {
      const expectedResponse = {
        reference_id: expect.anything(),
      };
      const userID = 5;
      const amount = 100;

      const saveSpy = jest
        .spyOn(walletRepository, 'save')
        .mockResolvedValueOnce();
      const createSpy = jest
        .spyOn(transactionRepository, 'create')
        .mockReturnValueOnce({});
      const saveTransactionSpy = jest
        .spyOn(transactionRepository, 'save')
        .mockResolvedValueOnce();

      jest.spyOn(walletRepository, 'findOne').mockResolvedValue({
        id: expect.anything(),
        userId: userID,
        balance: 50,
      });

      const result = await walletService.addMoney(userID, amount);

      expect(result).toEqual(expectedResponse);
      expect(saveSpy).toHaveBeenCalledTimes(1);
      expect(createSpy).toHaveBeenCalledTimes(1);
      expect(saveTransactionSpy).toHaveBeenCalledTimes(1);
    });

    it('should throw error', async () => {
      const userID = 5;
      const amount = 100;

      const findOneSpy = jest
        .spyOn(walletRepository, 'findOne')
        .mockResolvedValue({
          id: expect.anything(),
          userId: userID,
          balance: 50,
        });
      const saveSpy = jest.spyOn(walletRepository, 'save').mockRejectedValue({
        message: 'fake query',
        query: { data: userID, data2: amount },
      });
      const createSpy = jest.spyOn(transactionRepository, 'create');
      const saveTransactionSpy = jest.spyOn(transactionRepository, 'save');

      await expect(walletService.addMoney(userID, amount)).rejects.toThrow(
        new HttpException('transaction failed!', HttpStatus.BAD_REQUEST),
      );

      expect(findOneSpy).toHaveBeenCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledTimes(1);
      expect(createSpy).toHaveBeenCalledTimes(1);
      expect(saveTransactionSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('getTotalAmounts', () => {
    it('should return total amounts from transactions', async () => {
      const expectedTotal = 100;
      const queryRunner = {
        query: jest.fn().mockReturnValueOnce([{ total: 100 }]),
      };
      const mockConnection = {
        createQueryRunner: jest.fn().mockReturnValueOnce(queryRunner),
      };

      const service = new WalletService(
        walletRepository,
        transactionRepository,
        mockConnection as any,
      );

      const result = await walletService.getTotalAmounts();
      expect(result).toEqual(expectedTotal);
    });
  });
});
