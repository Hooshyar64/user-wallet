import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WalletModule } from './wallets/wallet.module';
import { Wallet } from './wallets/entities/wallet.entity';
import { Transaction } from './wallets/entities/Transaction.entity';
import { ScheduleModule } from '@nestjs/schedule';
import { TransactionJobModule } from './wallets/TransactionJob/transactionJob.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, envFilePath: '.env' }),
    ScheduleModule.forRoot(),
    TypeOrmModule.forFeature([Wallet, Transaction]),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [Wallet, Transaction],
      synchronize: true,
      dropSchema: false,
    }),
    WalletModule,
    TransactionJobModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
