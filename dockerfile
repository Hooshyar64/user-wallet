FROM node:16 //edit the version to yours
WORKDIR /computer_username/src/app  //edit the username to yours
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
EXPOSE 8080
CMD ["node", "dist/main"]